const data = require("./data-1.cjs");

function countAllPeople() {
  // let count=0;
  // for (let key of data['houses']) {
  //   count+=key['people'].length;
  // }
  // return count;
  return data.houses.reduce((sum, value) =>{
    sum+=value.people.length;
    return sum;
  }, 0);
}

function peopleByHouses() {
  // let names={};
  // for (let key of data.houses) {
  //     names[key.name] =key.people.length;
  // }
  // return names;
  // const names = {};

  // data.houses.forEach((house) => {
  //   names[house.name] = house.people.length;
  // });
  return data.houses.reduce((name,house)=>{
    name[house.name] = house.people.length;
    return name;
  },{})
}

function everyone() {
  // let names=[];
  // for (let key of data.houses) {
  //     for (let keys of key.people){
  //         names.push(keys.name);
  //     }
  // }
  // return names;
  // const names = [];

  // data.houses.forEach((house) => {
  //   house.people.forEach((person) => {
  //     names.push(person.name);
  //   });
  // });
  return data.houses.reduce((name,house)=>{
    name=name.concat(house.people.map((person)=>person.name))
    return name;
  },[])
}

function nameWithS() {
  // let names=[];
  // for (let key of data.houses) {
  //     for (let keys of key.people){
  //         if (keys.name.includes('s'||'S')){
  //             names.push(keys.name);
  //         }
  //     }
  // }
  // return names;
  // let names = [];
  // data.houses.forEach((house) => {
  //   let filteredPeople = house.people
  //     .filter(
  //       (person) => person.name.includes("s") || person.name.includes("S")
  //     )
  //     .map((person) => person.name);
  //   names = names.concat(filteredPeople);
  // });
  return data.houses.reduce((names,val)=>{
    names = names.concat(val.people.map((person)=>person.name))
    return names;
  },[]).filter((nam)=>{
    return nam.includes('s')|| nam.includes('S')});
}

function nameWithA() {
  // let names=[];
  // for (let key of data.houses) {
  //     for (let keys of key.people){
  //         if (keys.name.includes('a'||'A')){
  //             names.push(keys.name);
  //         }
  //     }
  // }
  // return names;
  // let names = [];
  // data.houses.forEach((house) => {
  //   let filteredPeople = house.people
  //     .filter(
  //       (person) => person.name.includes("a") || person.name.includes("A")
  //     )
  //     .map((person) => person.name);
  //   names = names.concat(filteredPeople);
  // });

  // return names;
  return data.houses.reduce((names,val)=>{
    names = names.concat(val.people.map((person)=>person.name))
    return names;
  },[]).filter((nam)=>{
    return nam.includes('a')|| nam.includes('A')});
}

function surnameWithS() {
  // let names=[];
  // for (let key of data.houses) {
  //     for (let keys of key.people){
  //         const surname = keys.name.split(' ')[1];
  //         if (surname.startsWith('S')){
  //             names.push(keys.name);
  //         }
  //     }
  // }
  // return names;
  // let names = [];
  // data.houses.forEach((house) => {
  //   house.people.forEach((person) => {
  //     const surname = person.name.split(" ")[1];
  //     if (surname.startsWith("S")) {
  //       names.push(person.name);
  //     }
  //   });
  // });
  // return names;
  return data.houses.reduce((names,val)=>{
    names=names.concat(val.people.map(person=>person.name))
    return names
  },[]).filter(nam=>{
    return nam.split(' ')[1].startsWith('S');
  })
}

function surnameWithA() {
  // let names=[];
  // for (let key of data.houses) {
  //     for (let keys of key.people){
  //         const surname = keys.name.split(' ')[1];
  //         if (surname.startsWith('A')){
  //             names.push(keys.name);
  //         }
  //     }
  // }
  // return names;
  // let names = [];
  // data.houses.forEach((house) => {
  //   house.people.forEach((person) => {
  //     const surname = person.name.split(" ")[1];
  //     if (surname.startsWith("A")) {
  //       names.push(person.name);
  //     }
  //   });
  // });
  // return names;
  return data.houses.reduce((names,val)=>{
    names=names.concat(val.people.map(person=>person.name))
    return names
  },[]).filter(nam=>{
    return nam.split(' ')[1].startsWith('A');
  })
}

function peopleNameOfAllHouses() {
  // let names={};
  // for (let key of data.houses) {
  //   let arr=[];
  //   for(let keys of key.people) {
  //     arr.push(keys.name);
  //   }
  //   names[key.name] = arr;
  // }
  // return names;
  // let names = {};
  // data.houses.forEach((house) => {
  //   let arr = [];
  //   house.people.forEach((person) => {
  //     arr.push(person.name);
  //   });
  //   names[house.name] = arr;
  // });
  // return names;
  return data.houses.reduce((names,val)=>{
    names[val.name]=val.people.map(person=>person.name);
    return names },{})
}

// Testing your result after writing your function
console.log(countAllPeople(data));
// Output should be 33

console.log(peopleByHouses(data));
// Output should be
//{Arryns: 1, Baratheons: 6, Dothrakis: 1, Freys: 1, Greyjoys: 3, Lannisters: 4,Redwyne: 1,Starks: 8,Targaryens: 2,Tullys: 4,Tyrells: 2}

console.log(everyone(data));
// Output should be
//["Eddard "Ned" Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon "Bran" Stark", "Rickon Stark", "Jon Snow", "Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Queen Cersei (Lannister) Baratheon", "King Robert Baratheon", "Stannis Baratheon", "Renly Baratheon", "Joffrey Baratheon", "Tommen Baratheon", "Myrcella Baratheon", "Daenerys Targaryen", "Viserys Targaryen", "Balon Greyjoy", "Theon Greyjoy", "Yara Greyjoy", "Margaery (Tyrell) Baratheon", "Loras Tyrell", "Catelyn (Tully) Stark", "Lysa (Tully) Arryn", "Edmure Tully", "Brynden Tully", "Olenna (Redwyne) Tyrell", "Walder Frey", "Jon Arryn", "Khal Drogo"]

console.log(nameWithS(data));
// Output should be
// ["Eddard "Ned" Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon "Bran" Stark", "Rickon Stark", "Jon Snow", "Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Queen Cersei (Lannister) Baratheon", "Stannis Baratheon", "Daenerys Targaryen", "Viserys Targaryen", "Loras Tyrell", "Catelyn (Tully) Stark", "Lysa (Tully) Arryn"]

console.log(nameWithA(data));
// Output should be
// ["Eddard Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon Stark", "Rickon Stark", "Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Cersei Baratheon", "Robert Baratheon", "Stannis Baratheon", "Renly Baratheon", "Joffrey Baratheon", "Tommen Baratheon", "Myrcella Baratheon", "Daenerys Targaryen", "Viserys Targaryen", "Balon Greyjoy", "Yara Greyjoy", "Margaery Baratheon", "Loras Tyrell", "Catelyn Stark", "Lysa Arryn", "Olenna Tyrell", "Walder Frey", "Jon Arryn", "Khal Drogo"]

console.log(surnameWithS(data));
// Output should be
// ["Eddard Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon Stark", "Rickon Stark", "Jon Snow", "Catelyn Stark"]

console.log(surnameWithA(data));
// Output should be
// ["Lysa Arryn", "Jon Arryn"]

console.log(peopleNameOfAllHouses(data));
// Output should be
// {Arryns: ["Jon Arryn"], Baratheons: ["Robert Baratheon", "Stannis Baratheon", "Renly Baratheon", "Joffrey Baratheon", "Tommen Baratheon", "Myrcella Baratheon"], Dothrakis: ["Khal Drogo"], Freys: ["Walder Frey"], Greyjoys: ["Balon Greyjoy", "Theon Greyjoy", "Yara Greyjoy"], Lannisters: ["Tywin Lannister", "Tyrion Lannister", "Jaime Lannister", "Cersei Baratheon"], Redwyne: ["Olenna Tyrell"], Starks: ["Eddard Stark", "Benjen Stark", "Robb Stark", "Sansa Stark", "Arya Stark", "Brandon Stark", "Rickon Stark", "Jon Snow"], Targaryens: ["Daenerys Targaryen", "Viserys Targaryen"], Tullys: ["Catelyn Stark", "Lysa Arryn", "Edmure Tully", "Brynden Tully"], Tyrells: ["Margaery Baratheon", "Loras Tyrell"]}
